
"     __  __ /\_\    ___ ___   _ __   ___
"    /\ \/\ \\/\ \ /' __` __`\/\`'__\/'___\
"  __\ \ \_/ |\ \ \/\ \/\ \/\ \ \ \//\ \__/
" /\_\\ \___/  \ \_\ \_\ \_\ \_\ \_\\ \____\
" \/_/ \/__/    \/_/\/_/\/_/\/_/\/_/ \/____/

" ----------------------------------------
" Basic settings
" ----------------------------------------
set nocompatible
set number
set relativenumber
set updatetime=1000
set wrap lbr
set mouse=a

filetype plugin indent on
filetype plugin on
syntax enable

" ----------------------------------------
" General
" ----------------------------------------
" Leader key
let mapleader = ";"

" Custom key binds
nnoremap <leader>w 	:w<CR> 
nnoremap <leader>q 	:wq<CR> 
inoremap jj 		<Esc>

" Switch between tabs
nnoremap <leader>h  :tabfirst<CR>
nnoremap <leader>j  :tabnext<CR>
nnoremap <leader>k  :tabprev<CR>
nnoremap <leader>l  :tablast<CR>

" Spellcheck
noremap <F7> :set spell! spell?<CR>

" Highlight search
noremap <F8> :set hlsearch! hlsearch?<CR>

" Sort function to key
vnoremap <leader>s 	:sort<CR>

" Easier moving of code blocks
vnoremap < <gv
vnoremap > >gv

" ----------------------------------------
" Finding
" ----------------------------------------
set path+=**
set wildmenu

" ----------------------------------------
" Tags
" ----------------------------------------
" Create 'tags' file (ctags required)
command! MakeTags !ctags -R .

" ----------------------------------------
" Snippets
" ----------------------------------------
nnoremap ,html :-1read $HOME/.vim/.skeleton.html<CR>3jwf>a

" ---------------------------------------
" Markdown
" ----------------------------------------
" Navigation
autocmd FileType markdown nnoremap j gj
autocmd FileType markdown nnoremap k gk
autocmd FileType markdown vnoremap j gj
autocmd FileType markdown vnoremap k gk

autocmd FileType markdown nnoremap <C-j> <down>
autocmd FileType markdown nnoremap <C-k> <up>
autocmd FileType markdown vnoremap <C-j> <down>
autocmd FileType markdown vnoremap <C-k> <up>

autocmd FileType markdown nnoremap <Down> 	:m .+1<CR>==
autocmd FileType markdown nnoremap <Up> 	:m .-2<CR>==
autocmd FileType markdown vnoremap <Down> 	:m '>+1<CR>gv=gv
autocmd FileType markdown vnoremap <Up> 	:m '<-2<CR>gv=gv

" ----------------------------------------
" R
" ----------------------------------------
autocmd FileType r set tabstop=2
autocmd FileType r set softtabstop=2
autocmd FileType r set expandtab
autocmd FileType r set shiftwidth=2
autocmd FileType r set smarttab
autocmd FileType r set colorcolumn=80

" ----------------------------------------
" Python
" ----------------------------------------
autocmd FileType python set expandtab
autocmd FileType python set textwidth=79
autocmd FileType python set tabstop=8
autocmd FileType python set softtabstop=4
autocmd FileType python set shiftwidth=4
autocmd FileType python set autoindent
autocmd FileType python set colorcolumn=80
autocmd FileType python :syntax on

autocmd FileType python vnoremap <silent> <F3> :w <bar> !python3.6<CR>
autocmd FileType python nnoremap <silent> <F3> :w <bar> !python3.6 %<CR>
autocmd FileType python vnoremap <silent> <F12> :w <bar> !python3.5<CR>
autocmd FileType python nnoremap <silent> <F12> :w <bar> !python3.5 %<CR>
autocmd FileType python vnoremap <silent> <F4> :w <bar> !python2.7<CR>
autocmd FileType python nnoremap <silent> <F4> :w <bar> !python2.7 %<CR>

autocmd FileType python nnoremap <Down> 	:m .+1<CR>==
autocmd FileType python nnoremap <Up> 		:m .-2<CR>==
autocmd FileType python vnoremap <Down> 	:m '>+1<CR>gv=gv
autocmd FileType python vnoremap <Up> 		:m '<-2<CR>gv=gv

